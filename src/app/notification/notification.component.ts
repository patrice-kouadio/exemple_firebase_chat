import {
  Component,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { NotificationModel } from '../models/notification-model';
import { PushNotificationService } from '../services/notis/push-notification.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class NotificationComponent implements OnInit, OnChanges {
  showPanel: boolean | undefined;
  notifications?: NotificationModel;
  notificationSub: Subscription | undefined;
  notificationTimeOute: any;

  constructor(private notificationsService: PushNotificationService) {}

  ngOnChanges(changes: SimpleChanges): void {
    this.getNotification();
  }

  ngOnInit(): void {
    this.getNotification();
  }
  getNotification() {
    this.notificationSub = this.notificationsService
      .getNotification()
      .subscribe((notification) => {
        this.notifications = notification;
        this.showPanel = notification !== null;
        this.notificationTimeOute = setTimeout(() => {
          this.showPanel = false;
        }, 3000);
        console.log('display notification', notification);
      });
  }

  dismissNotification() {
    this.showPanel = false;
  }

  ngOnDestroy(): void {
    this.notificationSub?.unsubscribe();
  }
}
