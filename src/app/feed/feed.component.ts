import { Component, OnChanges, OnInit } from '@angular/core';

import {ChatService} from '../services/chat.service';
import {Observable} from 'rxjs';
import { ChatMessage} from '../models/chat-message-model'
import { AngularFireList } from 'angularfire2/database';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit , OnChanges{

 feed: AngularFireList<ChatMessage[]>| any;

  constructor(private chat: ChatService) {

   }

  ngOnInit( ): void {
    const feedChat = this.chat.getMessages().valueChanges();
      feedChat.subscribe((datafeedChat: ChatMessage[]| any) =>{
      this.feed =  datafeedChat;

      })

  }
  ngOnChanges(){
    this.feed = this.chat.getMessages();
  }

}
