import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { User } from '../models/user-model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private user: Observable<firebase.User | any>;
  private authState: any;
  users: any;
  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFireDatabase,
    private router: Router
  ) {
    this.user = afAuth.authState;
  }

  authUser() {
    return this.user;
  }

  authuseverifie() {
    return this.afAuth;
  }

  get currenUserId(): string {
    return this.authState !== null ? this.authState.user.uid : '';
  }

  login(email: string, password: string) {
    return this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then((resolve: any) => {
        const status = 'online';
        this.setUserStatus(status);
        this.router.navigate(['chat']);
      });
  }

  signUp(email: string, password: string, displayName: string) {
    return this.afAuth.auth
      .createUserWithEmailAndPassword(email, password)
      .then((user: any) => {
        this.users = user;
        this.authState = user;

        const status = 'online';
        this.setUserData(email, displayName, status);
      })
      .catch((error: any) => console.log('error', error));
  }

  getSignUp() {}

  setUserData(email: string, displayName: string, status: string): void {
    const path = `users/${this.currenUserId}`;

    const data = {
      email: email,
      displayName: displayName,
      status: status,
    };
    this.db
      .object(path)
      .update(data)
      .catch((error) => console.log(error));
  }

  setUserStatus(status: string): void {
    const path = `users/${this.currenUserId}`;
    const data = {
      status: status,
    };
  }
}
