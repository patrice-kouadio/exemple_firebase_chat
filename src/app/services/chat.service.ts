import { query } from '@angular/animations';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';

import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import firebase from 'firebase';
import { from, Observable } from 'rxjs';

import { ChatMessage } from '../models/chat-message-model';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  user!: firebase.User;
  chatMessages!: AngularFireList<ChatMessage[]>;
  chatMessage!: ChatMessage;
  userName!: Observable<string>;

  constructor(
    private db: AngularFireDatabase,
    private afAuth: AngularFireAuth
  ) {
    this.afAuth.authState.subscribe((auth) => {
      if (auth !== undefined && auth !== null) {
        this.user = auth;
        console.log('auth of afAuth', auth);
      }

      this.getUser()
        .valueChanges()
        .subscribe((a: any) => {
          this.userName = a.displayName;
        });
    });
  }

  getUser() {
    const userId = this.user.uid;
    const path = `/users/${userId}`;
    return this.db.object(path);
  }
  getUsers() {
    const path = '/users';
    return this.db.list(path);
  }

  sendMessage(msg: string) {
    const timestamp = this.getTimeStamp();
    const email = this.user.email;
    this.chatMessages = this.getMessages();
    this.chatMessages.push([
      {
        message: msg,
        timeSent: timestamp as any,
        userName: this.userName,
        email: email,
      },
    ]);
  }

  getMessages(): AngularFireList<ChatMessage[]> {
    //query to create our message feed binding
    return this.db.list('messages', (ref) => {
      let q = ref.limitToLast(25).orderByKey();
      return q;
    });
  }

  getTimeStamp() {
    const now = new Date();
    const date =
      now.getUTCFullYear() +
      '/' +
      (now.getUTCMonth() + 1) +
      '/' +
      now.getUTCDate();
    const time =
      now.getUTCHours() +
      ':' +
      (now.getUTCMinutes() + 1) +
      ':' +
      now.getUTCSeconds();
    return date + ' ' + time;
  }
}
