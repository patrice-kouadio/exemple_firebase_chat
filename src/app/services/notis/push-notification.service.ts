import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireMessaging } from 'angularfire2/messaging';
import { BehaviorSubject } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { NotificationModel } from 'src/app/models/notification-model';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';

const SERVER_URL = 'http://localhost:8080/notifications';

@Injectable({
  providedIn: 'root',
})
export class PushNotificationService {
  baseUrl = '//fcm.googleapis.com//v1/projects/akilcab-chat/messages:send';
  ContentType = 'application/json';
  Authorization =
    'AAAAZfKq7yg:APA91bH9Uqknpcl1vEOeJG_3MkS9kWeMlWqUwSPGKpDZzPdTTJ1LMcY49sspBH-89HBraoGJrkgtU1tEP-WSm-EYWxx8vRpGxqL8sEoyYVrpUH3gjBD-68E5qRLqCMuPt3NobUBa0jbM';

  notification$: BehaviorSubject<NotificationModel> =
    new BehaviorSubject<NotificationModel>({ body: '', title: '' });
  // private baseUrl = 'http://localhost:8080/notifications';

  constructor(
    private authService: AuthService,
    readonly afMessaging: AngularFireMessaging,
    private http: HttpClient,
    private snackBar: MatSnackBar
  ) {}

  // private getTopicUrl(token: string | any) {
  //   const topic = `${environment.firebase.appId}_user_${this.authService.currenUserId}`;
  //   return `https://ext-push-notifications.cometchat.com/fcmtokens/${token}/topics/${topic}`;
  // }

  // enableNotifications() {
  //   return this.afMessaging.requestToken.pipe(
  //     map((token) => this.getTopicUrl(token)),
  //     switchMap((url) =>
  //       this.http.post(url, { appId: environment.firebase.appId })
  //     )
  //   );
  // }

  // listenForMessages() {
  //   this.afMessaging.messages.subscribe(
  //     ({ data }: { data: any } | any) =>
  //       this.snackBar.open(`${data.title} - ${data.alert}`, 'Close', {
  //         duration: 1000,
  //       }),
  //     (err) => console.log(err)
  //   );
  // }
  setNotification(notification: NotificationModel) {
    this.notification$.next(notification);
  }

  getNotification() {
    return this.notification$.asObservable();
  }

  // public sendSubscriptionToTheServer(subscription: any) {
  //   const httpOptions = {
  //     headers: new HttpHeaders({
  //       'Content-Type': 'application/json',
  //       Authorization:
  //         'AAAAZfKq7yg:APA91bH9Uqknpcl1vEOeJG_3MkS9kWeMlWqUwSPGKpDZzPdTTJ1LMcY49sspBH-89HBraoGJrkgtU1tEP-WSm-EYWxx8vRpGxqL8sEoyYVrpUH3gjBD-68E5qRLqCMuPt3NobUBa0jbM',
  //     }),
  //   };
  //   return this.http.post<NotificationModel>(
  //     this.baseUrl,
  //     subscription,
  //     httpOptions
  //   );
  // }
}
