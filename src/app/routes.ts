import { Component } from "@angular/core"
import { Routes } from "@angular/router"
import { ChatroomComponent } from "./chatroom/chatroom.component"
import { LoginFormComponent } from "./login-form/login-form.component"
import { SignupFormComponent } from "./signup-form/signup-form.component"


export const appRoutes: Routes = [
    { path: 'signup', component: SignupFormComponent  },
    {path: 'login' , component: LoginFormComponent },
    {path: 'chat' , component: ChatroomComponent },
    { path: '' , redirectTo: '/login' , pathMatch: 'full'}
]