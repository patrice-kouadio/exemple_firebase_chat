import { Component, Input, OnInit } from '@angular/core';

import{ ChatService} from '../services/chat.service';
import {AuthService} from '../services/auth.service';
import{ChatMessage} from '../models/chat-message-model';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  @Input() chatMessage: any;
   userEmail?:string;
   userName?:string;
   messageContent?: string;
   timeStamp?: Date = new Date();
   isOwnMessage!: boolean

  constructor() { }

  ngOnInit(  ): void {
    this.chatMessage.map((v: { message: string,timeSent: Date ,email:string ,userName:string}) => {
      this.messageContent = v.message ;
      this.timeStamp = v.timeSent;
      this.userEmail = v.email;
      this.userName =v.userName;
    })
  }

}
