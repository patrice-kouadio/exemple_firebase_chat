import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css'],
})
export class SignupFormComponent implements OnInit, OnChanges {
  email!: string;
  password!: string;
  displayName!: string;
  errorMsg!: string;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnChanges(changes: SimpleChanges): void {
    console.log('user ui', this.authService.users);
  }

  ngOnInit(): void {
    console.log('user ui', this.authService.users);
  }

  signUp() {
    const email = this.email;
    const password = this.password;
    const displayName = this.displayName;
    this.authService
      .signUp(email, password, displayName)
      .then((resolve: any) => this.router.navigate(['chat']))
      .catch((error: any) => (this.errorMsg = error.message));
  }
}
