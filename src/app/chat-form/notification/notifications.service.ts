// import { AuthService } from '&services/auth/auth.service';
// import { environment } from '&env/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireMessaging } from 'angularfire2/messaging'
//import { AngularFirestore } from '@angular/fire/firestore';

import { map, switchMap } from 'rxjs/operators';
//import{MatSnackBar} from '@angular/material/snack-bar'
import firebase from 'firebase';
//import * as firebase from 'firebase/app'
//import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  //message = firebase.messaging();
 // currentMessage = new BehaviorSubject(null)
  constructor(
   readonly afMessaging: AngularFireMessaging,
    readonly http: HttpClient,


  ) { };

  // enableNotifications() {
  //   return this.afMessaging.requestToken.pipe(
  //     map(token => this.getTopicUrl(token)),
  //     switchMap(url => this.http.post(url, { appId: environment.appId})),
  //   );
  // };

  // listenForMessages() {
  //   this.afMessaging.messages.subscribe(
  //     ({ data }: { data: any }) =>
  //       this.snackBar.open(`${data.title} - ${data.alert}`, 'Close', {
  //         duration: 1000,
  //       }),
  //     err => console.log(err),
  //   );
  // }

  // private getTopicUrl(token: string) {
  //   const topic = `${environment}_user_${this.authService.userInfo.id}`;
  //   return `https://ext-push-notifications.cometchat.com/fcmtokens/${token}/topics/${topic}`;
  // };

}
