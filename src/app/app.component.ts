import { MessagingService } from './shared/messaging.service';
import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { SwPush } from '@angular/service-worker';
// import * as Notifications from 'expo-notifications';
import { AngularFireMessaging } from 'angularfire2/messaging';
// import { AngularFireMessaging } from '@angular/fire/messaging';
import { AngularFirestore } from 'angularfire2/firestore';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as firebase from 'firebase';

import { PushNotificationService } from './services/notis/push-notification.service';
import { title } from 'process';
import { from, of } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';
import { take } from 'rxjs/operators';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnChanges {
  message: any;
  title = 'angular-push-notifications';
  // messaging = firebase.messaging();
  constructor(
    private swPush: SwPush,
    private pushService: PushNotificationService,
    private afFireStore: AngularFirestore,
    private afMessaging: AngularFireMessaging,
    private afAuth: AngularFireAuth,
    private db: AngularFireDatabase,
    private snackBar: MatSnackBar
  ) {}
  ngOnChanges(changes: SimpleChanges): void {
    this.listen();
    console.log('messages on firebase change');
  }
  ngOnInit() {
    this.requestPermission();
    this.listen();
  }

  private updateToken(token: any) {
    console.log('update executed');

    this.afAuth.authState.pipe(take(1)).subscribe(
      (user) => {
        console.log('user', user);
        if (!user) return;

        const path = `fcmTokens/${user.uid}`;
        const data = {
          token: token,
          email: user.email,
          id: '1',
        };
        console.log('update user ', user);
        console.log('update data', data);

        this.db.object(path).update(data);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  requestPermission() {
    this.afMessaging.requestToken.subscribe(
      (token: any) => {
        console.log('Permission granted !');
        console.log(token);
        this.updateToken(token);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  listen() {
    this.afMessaging.messaging.subscribe((e) => {
      console.log('messagin e', e);
    });
    this.afMessaging.messages.subscribe(
      (message: any) => {
        console.log('messages', message);

        this.pushService.setNotification({
          body: message.notification.body,
          title: message.notification.title,
        });
      },
      (error) => {
        console.error(error);
      }
    );
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.requestPermission();
    console.log('component has been destroyed');
  }
}
