export class ChatMessage{
    $key?: string;
    email?: string | any;
    userName?:string |any;
    message?: string;
    timeSent?: Date = new Date();
}