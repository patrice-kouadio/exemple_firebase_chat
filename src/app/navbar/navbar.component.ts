import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user!: Observable<firebase.User>;
  userEmail!: string;

  constructor( private authService: AuthService ) { }

  ngOnInit(): void {
    this.user = this.authService.authUser();
    this.user.subscribe((user: any) =>{
      if(user){
        this.userEmail = user.email;
      }
    });
  }
  login(){
    
  }
  logout(){
    
  }
}
