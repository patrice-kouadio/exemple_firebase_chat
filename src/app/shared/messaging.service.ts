import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { auth, firestore } from 'firebase';

// Device detector
import { DeviceDetectorService } from 'ngx-device-detector';
import { BehaviorSubject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class MessagingService {

  private _user: any = new BehaviorSubject(null);
  currentMessage = new BehaviorSubject(null);
  token:string| any;
  constructor(
    private afAuth: AngularFireAuth,
    private _afs: AngularFirestore,
    private deviceService: DeviceDetectorService,
    private AngularFireMessaging: AngularFireMessaging
     ) {
        this.afAuth.user.subscribe(async user => {
          // const token = 'return'
          try {
            this.AngularFireMessaging.messages.subscribe(
              (_messaging:any) => {
              _messaging.onMessage = _messaging.onMessage.bind(_messaging);
              _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
              }
              )
            this.updatePushToken(this.token);
          } catch (error) {
            console.log("Error Message ", error);
          }
        });
     }

     private setUser(user: any) {
      this._user.next(user);
    }

    public get user(): BehaviorSubject<any> {
      return this._user;
    }

    requestPermission() {
      this.AngularFireMessaging.requestToken.subscribe(
        (token) => {
          this.token = token
        console.log(token);
        },
        (err) => {
        }
      );
    }

    receiveMessage() {
      this.AngularFireMessaging.messages.subscribe(
        (payload: any) => {
        return this.currentMessage.next(payload);
      })
    }
     /**
 * Update the User's push token
 * @param token string
 */
  public async updatePushToken(token: string) {
    try {
      const devices = await this._afs.firestore.collection('Devices').where('token', '==', token).get();

      if (devices.empty) {
        const deviceInfo = this.deviceService.getDeviceInfo();
        const data = {
          token: token,
          userId: this._user._value.uid,
          deviceType: 'web',
          deviceInfo: {
            browser: deviceInfo.browser,
            userAgent: deviceInfo.userAgent
          },
          createdAt: firestore.FieldValue.serverTimestamp()
        };

        await this._afs.firestore.collection('Devices').add(data);
        console.log('New Device Added');
      } else {
        console.log('Already existing Device');
      }
    } catch (error) {
      console.log("Error Message", error);
    }
  }
}
