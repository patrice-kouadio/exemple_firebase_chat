// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here. Other Firebase libraries
// are not available in the service worker.
importScripts("https://www.gstatic.com/firebasejs/7.24.0/firebase-app.js");
importScripts(
    "https://www.gstatic.com/firebasejs/7.24.0/firebase-messaging.js"
);

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
firebase.initializeApp({
    apiKey: "AIzaSyAaG4xmVCu2Jn671iGPO_Uudt9pXgfA5ms",
    authDomain: "akilcab-chat.firebaseapp.com",
    databaseURL: "https://akilcab-chat-default-rtdb.firebaseio.com",
    projectId: "akilcab-chat",
    storageBucket: "akilcab-chat.appspot.com",
    messagingSenderId: "437862985512",
    appId: "1:437862985512:web:4124fd47cfb191a88daa1d",
    //measurementId: 'G-measurement-id',
});

const messaging = firebase.messaging();

messaging.onBackgroundMessage((payload) => {
    console.log("payload", payload);
});