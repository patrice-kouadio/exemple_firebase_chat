// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebase:{
    apiKey: 'AIzaSyAaG4xmVCu2Jn671iGPO_Uudt9pXgfA5ms',
    authDomain: 'akilcab-chat.firebaseapp.com',
    databaseURL: 'https://akilcab-chat-default-rtdb.firebaseio.com',
    projectId: 'akilcab-chat',
    storageBucket: 'akilcab-chat.appspot.com',
    messagingSenderId: '437862985512',
    appId: '1:437862985512:web:4124fd47cfb191a88daa1d'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
